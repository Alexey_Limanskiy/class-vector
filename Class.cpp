
#include <iostream>
#include <math.h>

class Vector
{
private:
    double x;
    double y;
    double z;
   

    public:
        Vector () : x(0) , y (0) , z (0)
        {}
        
        Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
        {}

      
        void Show()
        {
                   
            std:: cout << "x = "<< x << ", " << "y = "<< y << ", " << "z = " << z << ".\n";

            std::cout << "\n";

            std::cout << "Vector length = " << sqrt((x * x) + (y * y) + (z * z)) << "\n";
        }

    
      
};



int main()
{
     
    Vector v(20, 10, 5);
    v.Show ();




    return 0;
}